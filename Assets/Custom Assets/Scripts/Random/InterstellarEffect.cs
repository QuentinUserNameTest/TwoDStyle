﻿using UnityEngine;
using System.Collections;

public class InterstellarEffect : MonoBehaviour {

    private Transform trans;
    public float speedValue = 1.0f;
    public float continousAcceleration = 1.0f;
    public float exponetialAcceleration = 1.01f;

	// Use this for initialization
	void Start () {
        trans = this.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        trans.Rotate(0.0f, speedValue * Time.deltaTime, 0.0f);
        if (Time.time > 23.0f && Time.time < 23.0f)
        {
            speedValue *= 1 + (0.4f * Time.deltaTime);
        }
        else if (Time.time > 23.5f && Time.time < 45.0f)
        {
            speedValue *= 1 + (0.1f * Time.deltaTime);
        }
        else if (Time.time > 45.0f && Time.time < 155.0f)
        {
            speedValue *= 1 + (0.01f * Time.deltaTime);
        }
        else
        {
            speedValue *= 1 + (0.02f * Time.deltaTime);
        }
    }
}
