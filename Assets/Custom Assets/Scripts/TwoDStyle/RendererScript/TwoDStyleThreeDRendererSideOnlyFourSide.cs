﻿using UnityEngine;
using System.Collections;

namespace TwoDStyle
{
    public class TwoDStyleThreeDRendererSideOnlyFourSide : TwoDStyleThreeDRendererSideOnly
    {

        private const float heightSlide = 360.0f / 8.0f;
        //public float topViewAngle = 5.0f;


        // Update is called once per frame
        protected override void faceToShow(float yAngle)
        {
            /* // Debug Test
            if (yAngle < 0.0f)
            {
                Debug.Log("yAngle can't have a negative value in  script : " + this);
            } else */
            if (yAngle >= 7 * heightSlide || yAngle < heightSlide)
            {
                SetActualShowedState(showedState.front);
            }
            else if (yAngle < (3 * (360.0f / 8.0f)))
            {
                SetActualShowedState(showedState.right);
            }
            else if (yAngle < (5 * (360.0f / 8.0f)))
            {
                SetActualShowedState(showedState.bottom);
            }
            else
            {
                SetActualShowedState(showedState.left);
            }
        }
    }
}
